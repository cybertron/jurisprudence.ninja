# jurisprudence.ninja

Site web permettant d'exploiter l'opendata des décisions de justice

Le décret 2020-797, précisé par un arrêté du 28/04/2021, prévoit l'accès aux décisions de justice en opendata.
A l'instar de ce qu'a fait le projet societe.ninja, ce projet vise à exploiter cette nouvelle source de données pour la mettre à disposition des avocats et autres professions juridiques afin de faciliter leur travail quotidien.

Pour rappel, voici l'agenda de mise en ligne fixé par les textes :

Justice administrative :
- 30 septembre 2021 s’agissant des décisions du Conseil d’Etat ;
- 31 mars 2022 s’agissant des décisions des cours administratives d’appel ;
- 30 juin 2022 s’agissant des décisions des tribunaux administratifs.

Contentieux Civil commercial & social :
- 30 septembre 2021 s’agissant des décisions rendues par la Cour de cassation ;
- 30 avril 2022 s’agissant des décisions rendues par les cours d’appel ;
- 30 juin 2023 s’agissant des décisions rendues par les conseils de prud’hommes ;
- 31 décembre 2024 s’agissant des décisions rendues par les tribunaux de commerce ;
- 30 septembre 2025 s’agissant des décisions rendues par les tribunaux judiciaires.

Contentieux pénaux :
- 30 septembre 2021 s’agissant des décisions rendues par la Cour de cassation ;
- 31 décembre 2024 s’agissant des décisions rendues par les juridictions de premier degré en matière contraventionnelle et délictuelle ;
- 31 décembre 2025 s’agissant des décisions rendues par les cours d’appel en matière contraventionnelle et délictuelle ;
- 31 décembre 2025 s’agissant des décisions rendues en matière criminelle.
