const cacheName = "jp-ninja";
const assets = [
	"/index.html",
	"/decision.html",
	"/mentions.html",
	"/agenda.html",
	"/index.css",
	"/decision.css",
	"/app.js",
	"/taxonomy.js",
	"/config.js",
	"/version.js",
	"/images/waiter.gif",
	"/images/court.svg",
	"/images/border_color_black_24dp.svg",
	"/images/content_copy_black_24dp.svg"
]


self.addEventListener("install", installEvent => 
{
	installEvent.waitUntil(
		caches.open(cacheName).then(cache => 
		{
			cache.addAll(assets)
		})
  	)
})


self.addEventListener("fetch", fetchEvent => 
{
	fetchEvent.respondWith(
		caches.match(fetchEvent.request).then(res => 
		{
			return res || fetch(fetchEvent.request)
		})
	)
})


self.addEventListener('activate', (activateEvent) => 
{
	activateEvent.waitUntil(
		caches.keys().then((keyList) => 
		{
			return Promise.all(keyList.map((key) => 
			{
				if((cacheName).indexOf(key) === -1)
					return caches.delete(key);
			}));
		})
	);
});